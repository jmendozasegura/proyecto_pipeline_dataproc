import sys
from pyspark.sql import SparkSession

def main():
    spark = SparkSession.builder \
            .appName('DataTransformation')\
            .config('spark.jars.packages', 'com.google.cloud.spark:spark-bigquery-with-dependencies_2.12:0.15.1-beta') \
            .getOrCreate()

    print("[1] ------- Spark Session Created")

    country = sys.argv[1]

    print("[2] ------- Reading bq tables")

    table = "de-program-team-c:medium.covid_"+country.lower()
    df_covid_country = spark.read \
                            .format("bigquery") \
                            .option("table", table) \
                            .load()

    print("[3] ------- Transformations ")

    # Transformation 1
    df_covid_deaths = df_covid_country \
                      .selectExpr("max(date) as Fecha", "sum(daily_deaths) as TotalMuertes", "sum(daily_confirmed_cases) as TotalCasos")

    # Transformation 2
    df_covid_country_report = df_covid_deaths \
                              .selectExpr("*", "ROUND(TotalMuertes*100/TotalCasos,2) as RatioFallecidos")

    # GCS temporal
    gcs_bucket = 'bucket-prueba-jm'

    # Es necesario tener un dataset
    bq_dataset = 'medium'

    # BigQuery table que se creará o se sobreescribirá
    bq_table = 'covid_daily_kpi_'+country

    print("[4] ------- Saving bq tables")

    df_covid_country_report.write \
                   .format("bigquery") \
                   .option("table","{}.{}".format(bq_dataset, bq_table)) \
                   .option("temporaryGcsBucket", gcs_bucket) \
                   .mode('overwrite') \
                   .save()
  
main()
